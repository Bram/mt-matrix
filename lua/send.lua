function matrix.send_message(http_api, body, formatted_body, callback, txnId)
    if txnId == nil then
        txnId = os.time()
    end

    http_api.fetch({
        url = (
            config.url .. "/_matrix/client/v3/rooms/" .. config.room_id ..
            "/send/m.room.message/" .. txnId
        ),
        data = minetest.write_json({
            msgtype = "m.text",
            body = body,
            format = "org.matrix.custom.html",
            formatted_body = formatted_body
        }),
        extra_headers = { "Authorization: Bearer " .. config.access_token },
        method = "PUT"
    }, function(result)
        if result.code ~= 200 then
            minetest.debug("Failed to send message to Matrix homeserver with code " .. result.code)
            minetest.debug(result.data)
        else
            if callback ~= nil then
                callback(minetest.parse_json(result.data))
            end
        end
    end)
end

function matrix.send(http_api, strong_text, small_text, callback)
    small_text = small_text or ""
    strong_text = strong_text or ""

    return matrix.send_message(
        http_api, "**" .. strong_text .. "** " .. small_text,
        "<strong>" .. strong_text .. "</strong> " .. small_text,
        callback
    )
end

function matrix.say(http_api)
    return function(name, message)
        return matrix.send(http_api, name .. ":", message)
    end
end