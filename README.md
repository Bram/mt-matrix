# mt-matrix

This repository serves as a bridge between Matrix and Minetest servers.

## Setup

1. Clone this repository into your Minetest mods folder:

```sh
git clone https://git.noordstar.me/Bram/mt-matrix.git
```

2. To access the Matrix homeserver, this mod needs access to HTTP calls. Add this mod to your `minetest.conf` settings at the field `secure.http_mods`:

```
#    Comma-separated list of mods that are allowed to access HTTP APIs, which
#    allow them to upload and download data to/from the internet.
#    type: string
secure.http_mods=mt_matrix
```

3. Fill in the values in `config.lua`:

```lua
config = {

    -- The user one expects to log in as
    user = "@alice:example.org",

    -- The URL where the user communicates with the Matrix homeserver
    url = "https://matrix.example.org",

    -- Room ID that the Minetest server bridges to
    room_id = "!room_id:example.org",

    -- Access token to access the Matrix homeserver
    access_token = "",

    -- Average timeout duration
    sync_timeout = 30
}

```

4. Add the mod to your world configuration.

Now you're good to go! Feel free to create issues or send me a message.