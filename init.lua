-- Create code to load modules
modpath = minetest.get_modpath(minetest.get_current_modname())
local function load(name)
    dofile(modpath.."/"..name..".lua")
end

-- Load HTTP privileges
local http_api = minetest.request_http_api and minetest.request_http_api()
if http_api == nil then
    error(
        "This mod has no access to HTTP requests - which is crucial for it "..
        "to relay messages to Matrix. Please enable it at the "..
        "`secure.http_mods` or the `secure.trusted_mods` settings!"
    )
end

-- Load standard configuration
load("config")

-- Load Matrix modules
matrix = {}
load("lua/send")
load("lua/sync")

-- Mod Minetest server
minetest.register_on_chat_message(matrix.say(http_api))
minetest.register_on_joinplayer(function(ObjectRef, last_login)
    local name = ObjectRef:get_player_name()
    matrix.send(http_api, name .. " has joined the Minetest server")
end)
minetest.register_on_leaveplayer(function(ObjectRef, timed_out)
    local name = ObjectRef:get_player_name()
    matrix.send(http_api, name .. " has left the Minetest server")
end)
minetest.register_on_dieplayer(function(ObjectRef, reason)
    local name = ObjectRef:get_player_name()

    if reason.type == nil then
        matrix.send(http_api, name .. " died. Reason:", "unknown")
    elseif reason.type == "fall" then
        matrix.send(http_api, name .. " fell from a high place")
    elseif reason.type == "drown" then
        matrix.send(http_api, name .. " drowned")
    elseif reason.type == "punch" then
        local creature = "an enemy"
        local puncher = reason.object

        if puncher:is_player() then
            creature = puncher:get_player_name()
        else
            creature = puncher:get_entity_name()
        end

        matrix.send(http_api, name .. " was slain by " .. creature)
    else
        matrix.send(http_api, name .. " died. Reason:", reason.type)
    end
end)

-- Send achievements
if awards then
    awards.register_on_unlock(function(name, def)
        local award_name = def.name or def.title or "N.A."
        matrix.send(http_api, name .. "has made the achievement [" .. award_name .. "]")
    end)
end

-- Start Matrix sync
local names = {}

matrix.sync_forever(http_api, function(event)
    local name = names[event.sender] or event.sender
    
    if event.type == nil then
    elseif event.content == nil then
    elseif event.sender == config.user then
    elseif event.type == "m.room.message" then
        local msgtype = event.content.msgtype

        if msgtype == nil then
        elseif msgtype == "m.text" then
            if event.content.body ~= nil then
                minetest.chat_send_all(name .. ": " .. event.content.body)
            end
        elseif msgtype == "m.audio" then
            minetest.chat_send_all(name .. " sent an audio message")
        elseif msgtype == "m.video" then
            minetest.chat_send_all(name .. " sent a video")
        elseif msgtype == "m.image" then
            minetest.chat_send_all(name .. " sent an image")
        end
    elseif event.type == "m.room.member" then
        if event.content.displayname ~= nil and event.state_key ~= nil then
            names[event.state_key] = event.content.displayname
            
            minetest.chat_send_all("Adding Matrix user \"" .. names[event.state_key] .. "\" (" .. event.state_key .. ")")
        end
    end
end)
