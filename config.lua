config = {

    -- The user one expects to log in as
    user = "@alice:example.org",

    -- The URL where the user communicates with the Matrix homeserver
    url = "https://matrix.example.org",

    -- Room ID that the Minetest server bridges to
    room_id = "!room_id:example.org",

    -- Access token to access the Matrix homeserver
    access_token = "",

    -- Average timeout duration
    sync_timeout = 30
}

local function verify(key, expected_type, default_value)
    if config == nil then
        error("There is no config variable available!")
    elseif config[key] == nil then
        error("Config key " .. key .. " has not been set!")
    elseif type(config[key]) ~= expected_type then
        error(
            "Expected config key " .. key .. " to be of type " ..
            expected_type .. " but found type " .. type(config[key])
        )
    elseif config[key] == default_value then
        error(
            "Config key " .. key .. " is still the default (example) value. " ..
            "This implies that you have not configured it yet."
        )
    end
end

verify("user", "string", "@alice:example.org")
verify("url", "string", "https://matrix.example.org")
verify("room_id", "string", "!room_id:example.org")
verify("access_token", "string", "")
verify("sync_timeout", "number", nil)
